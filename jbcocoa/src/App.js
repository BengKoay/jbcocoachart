import moment from 'moment';
import axios from 'axios';
import aws4Interceptor from 'aws4-axios';
import React, { useState, useEffect } from 'react';
import { Line, Bar } from 'react-chartjs-2';
// import datepicker from 'js-datepicker';

const deviceID = '8926f120-ea58-4c19-a09b-bc2761319004';
const sensor1 = {
	name: 'Sensor No. 1',
	modelType: 'AirOne Plus',
	uuidSource: '8926f120-ea58-4c19-a09b-bc2761319004',
};
const sensor2 = {
	name: 'Sensor No. 2',
	modelType: 'AirOne Plus',
	uuidSource: '603bbed8-1e03-4168-b6bd-57c56685616a',
};
const sensor3 = {
	name: 'Sensor No. 3',
	modelType: 'AirOne Plus',
	uuidSource: '86604e2f-b27f-44d1-b1f4-4a05fecd1bd8',
};
const sensor4 = {
	name: 'Sensor No. 4',
	modelType: 'AirOne Plus',
	uuidSource: '8c36d9ce-72d5-4110-8a67-43028db559cc',
};
const sensor5 = {
	name: 'Sensor No. 5',
	modelType: 'AirOne Plus',
	uuidSource: '40b3884c-70f6-4d02-9f4d-86f5809019cb',
};
const sensor6 = {
	name: 'Sensor No. 6',
	modelType: 'AirOne Plus',
	uuidSource: 'cab43ede-80ea-4eb5-b851-38c2542badea',
};
const sensor7 = {
	name: 'Sensor No. 7',
	modelType: 'AirOne Plus',
	uuidSource: '614f7f72-744d-4267-b881-f73e80d7b699',
};
const sensor8 = {
	name: 'Sensor No. 8',
	modelType: 'AirOne Plus',
	uuidSource: '659f3809-1361-4b21-bc9a-1a63d17f0ab6',
};
const sensor9 = {
	name: 'Sensor No. 9',
	modelType: 'AirOne Plus',
	uuidSource: '4fe60fe8-d752-4e7c-bcc5-96c48a79d187',
};
const sensor10 = {
	name: 'Sensor No. 10',
	modelType: 'AirOne Plus',
	uuidSource: '967cad92-7b88-46f6-ae38-385eb8e4149d',
};
const sensor11 = {
	name: 'Sensor No. 11',
	modelType: 'AirOne Plus',
	uuidSource: '191400d7-fb9a-4f0b-9ef8-5f7fd064ebc2',
};
const sensor12 = {
	name: 'Sensor No. 12',
	modelType: 'AirOne Plus',
	uuidSource: 'd9ba978b-22ae-42d3-8170-88d198ffd61a',
};
const sensor13 = {
	name: 'Sensor No. 13',
	modelType: 'AirOne Plus',
	uuidSource: 'dee4a64e-b7d7-49f8-bf31-b5ccd5e86d1f',
};
const sensor14 = {
	name: 'Sensor No. 14',
	modelType: 'AirOne Plus',
	uuidSource: 'b984e055-a9f4-4fea-97ff-4bdd67f45877',
};
const sensor15 = {
	name: 'Sensor No. 1',
	modelType: 'AirOne Plus',
	uuidSource: 'a98e243b-1dfe-469e-b13f-94cff49864c5',
};
// const deviceID = 'b176cf1e-a537-46c9-8ae7-e7b27a3de5c7';
// const deviceID = '2befffc7-acbb-4c9a-b10a-9418f52fc10e';
const start = '1632326400';
const end = '1631792896';

const dateNow = `${moment().unix()}`;
const dateNowHumanRead = `${moment
	.unix(dateNow)
	.format('dddd, MMMM Do, YYYY h:mm:ss A')}`;
const date2DaysAgoFromNow = `${moment().subtract(2, 'days').unix()}`;
const date2DaysAgoFromNowHumanRead = `${moment
	.unix(date2DaysAgoFromNow)
	.format('dddd, MMMM Do, YYYY h:mm:ss A')}`;
const date3DaysAgoFromNow = `${moment().subtract(3, 'days').unix()}`;
const date3DaysAgoFromNowHumanRead = `${moment
	.unix(date3DaysAgoFromNow)
	.format('dddd, MMMM Do, YYYY h:mm:ss A')}`;
const date4DaysAgoFromNow = `${moment().subtract(4, 'days').unix()}`;
const date4DaysAgoFromNowHumanRead = `${moment
	.unix(date4DaysAgoFromNow)
	.format('dddd, MMMM Do, YYYY h:mm:ss A')}`;
const date5DaysAgoFromNow = `${moment().subtract(5, 'days').unix()}`;
const date5DaysAgoFromNowHumanRead = `${moment
	.unix(date5DaysAgoFromNow)
	.format('dddd, MMMM Do, YYYY h:mm:ss A')}`;
const date6DaysAgoFromNow = `${moment().subtract(6, 'days').unix()}`;
const date6DaysAgoFromNowHumanRead = `${moment
	.unix(date6DaysAgoFromNow)
	.format('dddd, MMMM Do, YYYY h:mm:ss A')}`;
const date7DaysAgoFromNow = `${moment().subtract(7, 'days').unix()}`;
const date7DaysAgoFromNowHumanRead = `${moment
	.unix(date7DaysAgoFromNow)
	.format('dddd, MMMM Do, YYYY h:mm:ss A')}`;
const date8DaysAgoFromNow = `${moment().subtract(8, 'days').unix()}`;
const date8DaysAgoFromNowHumanRead = `${moment
	.unix(date8DaysAgoFromNow)
	.format('dddd, MMMM Do, YYYY h:mm:ss A')}`;

const title = 'HumidityTemperature';
// const deviceId = 'c4af4daa-15fc-45fd-a922-e539f2c361f1';
const axiosSolar = {
	baseURL: 'https://9ry30zin92.execute-api.ap-southeast-1.amazonaws.com/public',
	apiKey: 'eJzWdiiQi18jzBIWxqFHV1TRAgfcIg8h8Lc0wgBB',
	region: 'ap-southeast-1',
	service: 'execute-api',
	accessKeyId: 'AKIAVBSYQGXZE6POVWVK',
	secretAccessKey: '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',
};
const axiosMainHelper = axios.create({
	baseURL: axiosSolar.baseURL,
	headers: { 'x-api-key': axiosSolar.apiKey },
});
const interceptor = aws4Interceptor(
	{
		region: axiosSolar.region,
		service: axiosSolar.service,
	},
	{
		accessKeyId: axiosSolar.accessKeyId,
		secretAccessKey: axiosSolar.secretAccessKey,
	}
);
axiosMainHelper.interceptors.request.use(interceptor);
const DynamicChart = () => {
	const [transactionData, setTransactionData] = useState([]);
	// const csvLink = useRef(); // setup the ref that we'll use for the hidden CsvLink click once we've updated the data
	const [chartData1, setChartData1] = useState({});
	const [chartData2, setChartData2] = useState({});
	const [chartData3, setChartData3] = useState({});
	const [chartData4, setChartData4] = useState({});
	const [chartData5, setChartData5] = useState({});
	const [chartData6, setChartData6] = useState({});
	const [chartData7, setChartData7] = useState({});
	const [chartData8, setChartData8] = useState({});
	const [chartData9, setChartData9] = useState({});
	const [chartData10, setChartData10] = useState({});
	const [chartData11, setChartData11] = useState({});
	const [chartData12, setChartData12] = useState({});
	const [chartData13, setChartData13] = useState({});
	const [chartData14, setChartData14] = useState({});
	const [chartData15, setChartData15] = useState({});
	const [employeeSalary, setEmployeeSalary] = useState([]);
	const [employeeAge, setEmployeeAge] = useState([]);

	const Chart = () => {
		let empSal = [];
		let empAge = [];
		let moisture = [];
		let quotient = [];
		let temperature = [];
		let timestamp = [];
		let humidex = [];
		let humidity = [];

		axiosMainHelper
			.get(
				`/rawtmpdata/?deviceID=${sensor1.uuidSource}&start_time=${moment()
					.startOf('day')
					.unix()}000&end_time=${dateNow}000`
				// `/rawtmpdata/?deviceID=${deviceId}`
			)
			.then((res) => {
				console.log('res', res);
				for (const row in res.data) {
					try {
						let eachDataPoint = JSON.parse(
							res.data[`${row}`].stringifiedData.S
						);
						console.log('eachDataPoint', eachDataPoint);
						if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor[0].hasOwnProperty('quotient') &&
							eachDataPoint.sensor[0].hasOwnProperty('moisture') &&
							eachDataPoint.sensor[0].hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor[0]', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor[0].moisture);
							quotient.push(eachDataPoint.sensor[0].quotient);
							temperature.push(eachDataPoint.sensor[0].temperature);
							console.log('moisture', moisture);
							// console.log('timestamp', timestamp);
						} else if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor.hasOwnProperty('quotient') &&
							eachDataPoint.sensor.hasOwnProperty('moisture') &&
							eachDataPoint.sensor.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor.moisture);
							quotient.push(eachDataPoint.sensor.quotient);
							temperature.push(eachDataPoint.sensor.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('quotient') &&
							eachDataPoint.hasOwnProperty('moisture') &&
							eachDataPoint.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.moisture);
							quotient.push(eachDataPoint.quotient);
							temperature.push(eachDataPoint.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('temperature') &&
							eachDataPoint.hasOwnProperty('humidity') &&
							eachDataPoint.hasOwnProperty('humidex')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							temperature.push(eachDataPoint.temperature);
							humidity.push(eachDataPoint.humidity);
							humidex.push(eachDataPoint.humidex);
							console.log('moisture', moisture);
						}
					} catch (e) {
						console.log(e);
					}
				}
				// const data = JSON.parse(res.data[`${row}`].stringifiedData.S);
				// for (const dataObj of res) {
				// 	empSal.push(parseInt(dataObj.employee_salary));
				// 	empAge.push(parseInt(dataObj.employee_age));
				// }
				setChartData1({
					labels: timestamp,
					datasets: [
						{
							label: 'temperature',
							data: temperature,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
						{
							label: 'humidity',
							data: humidity,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
					],
				});
			})
			.catch((err) => {
				console.log(err);
			});
	};

	const Chart2 = () => {
		let empSal = [];
		let empAge = [];
		let moisture = [];
		let quotient = [];
		let temperature = [];
		let timestamp = [];
		let humidex = [];
		let humidity = [];

		axiosMainHelper
			.get(
				`/rawtmpdata/?deviceID=${sensor2.uuidSource}&start_time=${moment()
					.startOf('day')
					.unix()}000&end_time=${dateNow}000`
				// `/rawtmpdata/?deviceID=${deviceId}`
			)
			.then((res) => {
				console.log('res', res);
				for (const row in res.data) {
					try {
						let eachDataPoint = JSON.parse(
							res.data[`${row}`].stringifiedData.S
						);
						console.log('eachDataPoint', eachDataPoint);
						if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor[0].hasOwnProperty('quotient') &&
							eachDataPoint.sensor[0].hasOwnProperty('moisture') &&
							eachDataPoint.sensor[0].hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor[0]', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor[0].moisture);
							quotient.push(eachDataPoint.sensor[0].quotient);
							temperature.push(eachDataPoint.sensor[0].temperature);
							console.log('moisture', moisture);
							// console.log('timestamp', timestamp);
						} else if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor.hasOwnProperty('quotient') &&
							eachDataPoint.sensor.hasOwnProperty('moisture') &&
							eachDataPoint.sensor.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor.moisture);
							quotient.push(eachDataPoint.sensor.quotient);
							temperature.push(eachDataPoint.sensor.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('quotient') &&
							eachDataPoint.hasOwnProperty('moisture') &&
							eachDataPoint.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.moisture);
							quotient.push(eachDataPoint.quotient);
							temperature.push(eachDataPoint.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('temperature') &&
							eachDataPoint.hasOwnProperty('humidity') &&
							eachDataPoint.hasOwnProperty('humidex')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							temperature.push(eachDataPoint.temperature);
							humidity.push(eachDataPoint.humidity);
							humidex.push(eachDataPoint.humidex);
							console.log('moisture', moisture);
						}
					} catch (e) {
						console.log(e);
					}
				}
				// const data = JSON.parse(res.data[`${row}`].stringifiedData.S);
				// for (const dataObj of res) {
				// 	empSal.push(parseInt(dataObj.employee_salary));
				// 	empAge.push(parseInt(dataObj.employee_age));
				// }
				setChartData2({
					labels: timestamp,
					datasets: [
						{
							label: 'temperature',
							data: temperature,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
						{
							label: 'humidity',
							data: humidity,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
					],
				});
			})
			.catch((err) => {
				console.log(err);
			});
	};
	const Chart3 = () => {
		let empSal = [];
		let empAge = [];
		let moisture = [];
		let quotient = [];
		let temperature = [];
		let timestamp = [];
		let humidex = [];
		let humidity = [];

		axiosMainHelper
			.get(
				`/rawtmpdata/?deviceID=${sensor3.uuidSource}&start_time=${moment()
					.startOf('day')
					.unix()}000&end_time=${dateNow}000`
				// `/rawtmpdata/?deviceID=${deviceId}`
			)
			.then((res) => {
				console.log('res', res);
				for (const row in res.data) {
					try {
						let eachDataPoint = JSON.parse(
							res.data[`${row}`].stringifiedData.S
						);
						console.log('eachDataPoint', eachDataPoint);
						if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor[0].hasOwnProperty('quotient') &&
							eachDataPoint.sensor[0].hasOwnProperty('moisture') &&
							eachDataPoint.sensor[0].hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor[0]', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor[0].moisture);
							quotient.push(eachDataPoint.sensor[0].quotient);
							temperature.push(eachDataPoint.sensor[0].temperature);
							console.log('moisture', moisture);
							// console.log('timestamp', timestamp);
						} else if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor.hasOwnProperty('quotient') &&
							eachDataPoint.sensor.hasOwnProperty('moisture') &&
							eachDataPoint.sensor.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor.moisture);
							quotient.push(eachDataPoint.sensor.quotient);
							temperature.push(eachDataPoint.sensor.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('quotient') &&
							eachDataPoint.hasOwnProperty('moisture') &&
							eachDataPoint.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.moisture);
							quotient.push(eachDataPoint.quotient);
							temperature.push(eachDataPoint.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('temperature') &&
							eachDataPoint.hasOwnProperty('humidity') &&
							eachDataPoint.hasOwnProperty('humidex')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							temperature.push(eachDataPoint.temperature);
							humidity.push(eachDataPoint.humidity);
							humidex.push(eachDataPoint.humidex);
							console.log('moisture', moisture);
						}
					} catch (e) {
						console.log(e);
					}
				}
				// const data = JSON.parse(res.data[`${row}`].stringifiedData.S);
				// for (const dataObj of res) {
				// 	empSal.push(parseInt(dataObj.employee_salary));
				// 	empAge.push(parseInt(dataObj.employee_age));
				// }
				setChartData3({
					labels: timestamp,
					datasets: [
						{
							label: 'temperature',
							data: temperature,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
						{
							label: 'humidity',
							data: humidity,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
					],
				});
			})
			.catch((err) => {
				console.log(err);
			});
	};
	const Chart4 = () => {
		let empSal = [];
		let empAge = [];
		let moisture = [];
		let quotient = [];
		let temperature = [];
		let timestamp = [];
		let humidex = [];
		let humidity = [];

		axiosMainHelper
			.get(
				`/rawtmpdata/?deviceID=${sensor4.uuidSource}&start_time=${moment()
					.startOf('day')
					.unix()}000&end_time=${dateNow}000`
				// `/rawtmpdata/?deviceID=${deviceId}`
			)
			.then((res) => {
				console.log('res', res);
				for (const row in res.data) {
					try {
						let eachDataPoint = JSON.parse(
							res.data[`${row}`].stringifiedData.S
						);
						console.log('eachDataPoint', eachDataPoint);
						if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor[0].hasOwnProperty('quotient') &&
							eachDataPoint.sensor[0].hasOwnProperty('moisture') &&
							eachDataPoint.sensor[0].hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor[0]', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor[0].moisture);
							quotient.push(eachDataPoint.sensor[0].quotient);
							temperature.push(eachDataPoint.sensor[0].temperature);
							console.log('moisture', moisture);
							// console.log('timestamp', timestamp);
						} else if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor.hasOwnProperty('quotient') &&
							eachDataPoint.sensor.hasOwnProperty('moisture') &&
							eachDataPoint.sensor.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor.moisture);
							quotient.push(eachDataPoint.sensor.quotient);
							temperature.push(eachDataPoint.sensor.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('quotient') &&
							eachDataPoint.hasOwnProperty('moisture') &&
							eachDataPoint.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.moisture);
							quotient.push(eachDataPoint.quotient);
							temperature.push(eachDataPoint.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('temperature') &&
							eachDataPoint.hasOwnProperty('humidity') &&
							eachDataPoint.hasOwnProperty('humidex')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							temperature.push(eachDataPoint.temperature);
							humidity.push(eachDataPoint.humidity);
							humidex.push(eachDataPoint.humidex);
							console.log('moisture', moisture);
						}
					} catch (e) {
						console.log(e);
					}
				}
				// const data = JSON.parse(res.data[`${row}`].stringifiedData.S);
				// for (const dataObj of res) {
				// 	empSal.push(parseInt(dataObj.employee_salary));
				// 	empAge.push(parseInt(dataObj.employee_age));
				// }
				setChartData4({
					labels: timestamp,
					datasets: [
						{
							label: 'temperature',
							data: temperature,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
						{
							label: 'humidity',
							data: humidity,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
					],
				});
			})
			.catch((err) => {
				console.log(err);
			});
	};
	const Chart5 = () => {
		let empSal = [];
		let empAge = [];
		let moisture = [];
		let quotient = [];
		let temperature = [];
		let timestamp = [];
		let humidex = [];
		let humidity = [];

		axiosMainHelper
			.get(
				`/rawtmpdata/?deviceID=${sensor5.uuidSource}&start_time=${moment()
					.startOf('day')
					.unix()}000&end_time=${dateNow}000`
				// `/rawtmpdata/?deviceID=${deviceId}`
			)
			.then((res) => {
				console.log('res', res);
				for (const row in res.data) {
					try {
						let eachDataPoint = JSON.parse(
							res.data[`${row}`].stringifiedData.S
						);
						console.log('eachDataPoint', eachDataPoint);
						if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor[0].hasOwnProperty('quotient') &&
							eachDataPoint.sensor[0].hasOwnProperty('moisture') &&
							eachDataPoint.sensor[0].hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor[0]', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor[0].moisture);
							quotient.push(eachDataPoint.sensor[0].quotient);
							temperature.push(eachDataPoint.sensor[0].temperature);
							console.log('moisture', moisture);
							// console.log('timestamp', timestamp);
						} else if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor.hasOwnProperty('quotient') &&
							eachDataPoint.sensor.hasOwnProperty('moisture') &&
							eachDataPoint.sensor.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor.moisture);
							quotient.push(eachDataPoint.sensor.quotient);
							temperature.push(eachDataPoint.sensor.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('quotient') &&
							eachDataPoint.hasOwnProperty('moisture') &&
							eachDataPoint.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.moisture);
							quotient.push(eachDataPoint.quotient);
							temperature.push(eachDataPoint.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('temperature') &&
							eachDataPoint.hasOwnProperty('humidity') &&
							eachDataPoint.hasOwnProperty('humidex')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							temperature.push(eachDataPoint.temperature);
							humidity.push(eachDataPoint.humidity);
							humidex.push(eachDataPoint.humidex);
							console.log('moisture', moisture);
						}
					} catch (e) {
						console.log(e);
					}
				}
				// const data = JSON.parse(res.data[`${row}`].stringifiedData.S);
				// for (const dataObj of res) {
				// 	empSal.push(parseInt(dataObj.employee_salary));
				// 	empAge.push(parseInt(dataObj.employee_age));
				// }
				setChartData5({
					labels: timestamp,
					datasets: [
						{
							label: 'temperature',
							data: temperature,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
						{
							label: 'humidity',
							data: humidity,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
					],
				});
			})
			.catch((err) => {
				console.log(err);
			});
	};
	const Chart6 = () => {
		let empSal = [];
		let empAge = [];
		let moisture = [];
		let quotient = [];
		let temperature = [];
		let timestamp = [];
		let humidex = [];
		let humidity = [];

		axiosMainHelper
			.get(
				`/rawtmpdata/?deviceID=${sensor6.uuidSource}&start_time=${moment()
					.startOf('day')
					.unix()}000&end_time=${dateNow}000`
				// `/rawtmpdata/?deviceID=${deviceId}`
			)
			.then((res) => {
				console.log('res', res);
				for (const row in res.data) {
					try {
						let eachDataPoint = JSON.parse(
							res.data[`${row}`].stringifiedData.S
						);
						console.log('eachDataPoint', eachDataPoint);
						if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor[0].hasOwnProperty('quotient') &&
							eachDataPoint.sensor[0].hasOwnProperty('moisture') &&
							eachDataPoint.sensor[0].hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor[0]', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor[0].moisture);
							quotient.push(eachDataPoint.sensor[0].quotient);
							temperature.push(eachDataPoint.sensor[0].temperature);
							console.log('moisture', moisture);
							// console.log('timestamp', timestamp);
						} else if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor.hasOwnProperty('quotient') &&
							eachDataPoint.sensor.hasOwnProperty('moisture') &&
							eachDataPoint.sensor.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor.moisture);
							quotient.push(eachDataPoint.sensor.quotient);
							temperature.push(eachDataPoint.sensor.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('quotient') &&
							eachDataPoint.hasOwnProperty('moisture') &&
							eachDataPoint.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.moisture);
							quotient.push(eachDataPoint.quotient);
							temperature.push(eachDataPoint.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('temperature') &&
							eachDataPoint.hasOwnProperty('humidity') &&
							eachDataPoint.hasOwnProperty('humidex')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							temperature.push(eachDataPoint.temperature);
							humidity.push(eachDataPoint.humidity);
							humidex.push(eachDataPoint.humidex);
							console.log('moisture', moisture);
						}
					} catch (e) {
						console.log(e);
					}
				}
				// const data = JSON.parse(res.data[`${row}`].stringifiedData.S);
				// for (const dataObj of res) {
				// 	empSal.push(parseInt(dataObj.employee_salary));
				// 	empAge.push(parseInt(dataObj.employee_age));
				// }
				setChartData6({
					labels: timestamp,
					datasets: [
						{
							label: 'temperature',
							data: temperature,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
						{
							label: 'humidity',
							data: humidity,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
					],
				});
			})
			.catch((err) => {
				console.log(err);
			});
	};
	const Chart7 = () => {
		let empSal = [];
		let empAge = [];
		let moisture = [];
		let quotient = [];
		let temperature = [];
		let timestamp = [];
		let humidex = [];
		let humidity = [];

		axiosMainHelper
			.get(
				`/rawtmpdata/?deviceID=${sensor7.uuidSource}&start_time=${moment()
					.startOf('day')
					.unix()}000&end_time=${dateNow}000`
				// `/rawtmpdata/?deviceID=${deviceId}`
			)
			.then((res) => {
				console.log('res', res);
				for (const row in res.data) {
					try {
						let eachDataPoint = JSON.parse(
							res.data[`${row}`].stringifiedData.S
						);
						console.log('eachDataPoint', eachDataPoint);
						if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor[0].hasOwnProperty('quotient') &&
							eachDataPoint.sensor[0].hasOwnProperty('moisture') &&
							eachDataPoint.sensor[0].hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor[0]', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor[0].moisture);
							quotient.push(eachDataPoint.sensor[0].quotient);
							temperature.push(eachDataPoint.sensor[0].temperature);
							console.log('moisture', moisture);
							// console.log('timestamp', timestamp);
						} else if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor.hasOwnProperty('quotient') &&
							eachDataPoint.sensor.hasOwnProperty('moisture') &&
							eachDataPoint.sensor.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor.moisture);
							quotient.push(eachDataPoint.sensor.quotient);
							temperature.push(eachDataPoint.sensor.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('quotient') &&
							eachDataPoint.hasOwnProperty('moisture') &&
							eachDataPoint.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.moisture);
							quotient.push(eachDataPoint.quotient);
							temperature.push(eachDataPoint.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('temperature') &&
							eachDataPoint.hasOwnProperty('humidity') &&
							eachDataPoint.hasOwnProperty('humidex')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							temperature.push(eachDataPoint.temperature);
							humidity.push(eachDataPoint.humidity);
							humidex.push(eachDataPoint.humidex);
							console.log('moisture', moisture);
						}
					} catch (e) {
						console.log(e);
					}
				}
				// const data = JSON.parse(res.data[`${row}`].stringifiedData.S);
				// for (const dataObj of res) {
				// 	empSal.push(parseInt(dataObj.employee_salary));
				// 	empAge.push(parseInt(dataObj.employee_age));
				// }
				setChartData7({
					labels: timestamp,
					datasets: [
						{
							label: 'temperature',
							data: temperature,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
						{
							label: 'humidity',
							data: humidity,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
					],
				});
			})
			.catch((err) => {
				console.log(err);
			});
	};
	const Chart8 = () => {
		let empSal = [];
		let empAge = [];
		let moisture = [];
		let quotient = [];
		let temperature = [];
		let timestamp = [];
		let humidex = [];
		let humidity = [];

		axiosMainHelper
			.get(
				`/rawtmpdata/?deviceID=${sensor8.uuidSource}&start_time=${moment()
					.startOf('day')
					.unix()}000&end_time=${dateNow}000`
				// `/rawtmpdata/?deviceID=${deviceId}`
			)
			.then((res) => {
				console.log('res', res);
				for (const row in res.data) {
					try {
						let eachDataPoint = JSON.parse(
							res.data[`${row}`].stringifiedData.S
						);
						console.log('eachDataPoint', eachDataPoint);
						if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor[0].hasOwnProperty('quotient') &&
							eachDataPoint.sensor[0].hasOwnProperty('moisture') &&
							eachDataPoint.sensor[0].hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor[0]', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor[0].moisture);
							quotient.push(eachDataPoint.sensor[0].quotient);
							temperature.push(eachDataPoint.sensor[0].temperature);
							console.log('moisture', moisture);
							// console.log('timestamp', timestamp);
						} else if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor.hasOwnProperty('quotient') &&
							eachDataPoint.sensor.hasOwnProperty('moisture') &&
							eachDataPoint.sensor.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor.moisture);
							quotient.push(eachDataPoint.sensor.quotient);
							temperature.push(eachDataPoint.sensor.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('quotient') &&
							eachDataPoint.hasOwnProperty('moisture') &&
							eachDataPoint.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.moisture);
							quotient.push(eachDataPoint.quotient);
							temperature.push(eachDataPoint.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('temperature') &&
							eachDataPoint.hasOwnProperty('humidity') &&
							eachDataPoint.hasOwnProperty('humidex')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							temperature.push(eachDataPoint.temperature);
							humidity.push(eachDataPoint.humidity);
							humidex.push(eachDataPoint.humidex);
							console.log('moisture', moisture);
						}
					} catch (e) {
						console.log(e);
					}
				}
				// const data = JSON.parse(res.data[`${row}`].stringifiedData.S);
				// for (const dataObj of res) {
				// 	empSal.push(parseInt(dataObj.employee_salary));
				// 	empAge.push(parseInt(dataObj.employee_age));
				// }
				setChartData8({
					labels: timestamp,
					datasets: [
						{
							label: 'temperature',
							data: temperature,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
						{
							label: 'humidity',
							data: humidity,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
					],
				});
			})
			.catch((err) => {
				console.log(err);
			});
	};

	const Chart9 = () => {
		let empSal = [];
		let empAge = [];
		let moisture = [];
		let quotient = [];
		let temperature = [];
		let timestamp = [];
		let humidex = [];
		let humidity = [];

		axiosMainHelper
			.get(
				`/rawtmpdata/?deviceID=${sensor9.uuidSource}&start_time=${moment()
					.startOf('day')
					.unix()}000&end_time=${dateNow}000`
				// `/rawtmpdata/?deviceID=${deviceId}`
			)
			.then((res) => {
				console.log('res', res);
				for (const row in res.data) {
					try {
						let eachDataPoint = JSON.parse(
							res.data[`${row}`].stringifiedData.S
						);
						console.log('eachDataPoint', eachDataPoint);
						if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor[0].hasOwnProperty('quotient') &&
							eachDataPoint.sensor[0].hasOwnProperty('moisture') &&
							eachDataPoint.sensor[0].hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor[0]', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor[0].moisture);
							quotient.push(eachDataPoint.sensor[0].quotient);
							temperature.push(eachDataPoint.sensor[0].temperature);
							console.log('moisture', moisture);
							// console.log('timestamp', timestamp);
						} else if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor.hasOwnProperty('quotient') &&
							eachDataPoint.sensor.hasOwnProperty('moisture') &&
							eachDataPoint.sensor.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor.moisture);
							quotient.push(eachDataPoint.sensor.quotient);
							temperature.push(eachDataPoint.sensor.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('quotient') &&
							eachDataPoint.hasOwnProperty('moisture') &&
							eachDataPoint.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.moisture);
							quotient.push(eachDataPoint.quotient);
							temperature.push(eachDataPoint.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('temperature') &&
							eachDataPoint.hasOwnProperty('humidity') &&
							eachDataPoint.hasOwnProperty('humidex')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							temperature.push(eachDataPoint.temperature);
							humidity.push(eachDataPoint.humidity);
							humidex.push(eachDataPoint.humidex);
							console.log('moisture', moisture);
						}
					} catch (e) {
						console.log(e);
					}
				}
				// const data = JSON.parse(res.data[`${row}`].stringifiedData.S);
				// for (const dataObj of res) {
				// 	empSal.push(parseInt(dataObj.employee_salary));
				// 	empAge.push(parseInt(dataObj.employee_age));
				// }
				setChartData9({
					labels: timestamp,
					datasets: [
						{
							label: 'temperature',
							data: temperature,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
						{
							label: 'humidity',
							data: humidity,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
					],
				});
			})
			.catch((err) => {
				console.log(err);
			});
	};

	const Chart10 = () => {
		let empSal = [];
		let empAge = [];
		let moisture = [];
		let quotient = [];
		let temperature = [];
		let timestamp = [];
		let humidex = [];
		let humidity = [];

		axiosMainHelper
			.get(
				`/rawtmpdata/?deviceID=${sensor10.uuidSource}&start_time=${moment()
					.startOf('day')
					.unix()}000&end_time=${dateNow}000`
				// `/rawtmpdata/?deviceID=${deviceId}`
			)
			.then((res) => {
				console.log('res', res);
				for (const row in res.data) {
					try {
						let eachDataPoint = JSON.parse(
							res.data[`${row}`].stringifiedData.S
						);
						console.log('eachDataPoint', eachDataPoint);
						if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor[0].hasOwnProperty('quotient') &&
							eachDataPoint.sensor[0].hasOwnProperty('moisture') &&
							eachDataPoint.sensor[0].hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor[0]', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor[0].moisture);
							quotient.push(eachDataPoint.sensor[0].quotient);
							temperature.push(eachDataPoint.sensor[0].temperature);
							console.log('moisture', moisture);
							// console.log('timestamp', timestamp);
						} else if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor.hasOwnProperty('quotient') &&
							eachDataPoint.sensor.hasOwnProperty('moisture') &&
							eachDataPoint.sensor.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor.moisture);
							quotient.push(eachDataPoint.sensor.quotient);
							temperature.push(eachDataPoint.sensor.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('quotient') &&
							eachDataPoint.hasOwnProperty('moisture') &&
							eachDataPoint.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.moisture);
							quotient.push(eachDataPoint.quotient);
							temperature.push(eachDataPoint.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('temperature') &&
							eachDataPoint.hasOwnProperty('humidity') &&
							eachDataPoint.hasOwnProperty('humidex')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							temperature.push(eachDataPoint.temperature);
							humidity.push(eachDataPoint.humidity);
							humidex.push(eachDataPoint.humidex);
							console.log('moisture', moisture);
						}
					} catch (e) {
						console.log(e);
					}
				}
				// const data = JSON.parse(res.data[`${row}`].stringifiedData.S);
				// for (const dataObj of res) {
				// 	empSal.push(parseInt(dataObj.employee_salary));
				// 	empAge.push(parseInt(dataObj.employee_age));
				// }
				setChartData10({
					labels: timestamp,
					datasets: [
						{
							label: 'temperature',
							data: temperature,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
						{
							label: 'humidity',
							data: humidity,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
					],
				});
			})
			.catch((err) => {
				console.log(err);
			});
	};

	const Chart11 = () => {
		let empSal = [];
		let empAge = [];
		let moisture = [];
		let quotient = [];
		let temperature = [];
		let timestamp = [];
		let humidex = [];
		let humidity = [];

		axiosMainHelper
			.get(
				`/rawtmpdata/?deviceID=${sensor11.uuidSource}&start_time=${moment()
					.startOf('day')
					.unix()}000&end_time=${dateNow}000`
				// `/rawtmpdata/?deviceID=${deviceId}`
			)
			.then((res) => {
				console.log('res', res);
				for (const row in res.data) {
					try {
						let eachDataPoint = JSON.parse(
							res.data[`${row}`].stringifiedData.S
						);
						console.log('eachDataPoint', eachDataPoint);
						if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor[0].hasOwnProperty('quotient') &&
							eachDataPoint.sensor[0].hasOwnProperty('moisture') &&
							eachDataPoint.sensor[0].hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor[0]', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor[0].moisture);
							quotient.push(eachDataPoint.sensor[0].quotient);
							temperature.push(eachDataPoint.sensor[0].temperature);
							console.log('moisture', moisture);
							// console.log('timestamp', timestamp);
						} else if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor.hasOwnProperty('quotient') &&
							eachDataPoint.sensor.hasOwnProperty('moisture') &&
							eachDataPoint.sensor.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor.moisture);
							quotient.push(eachDataPoint.sensor.quotient);
							temperature.push(eachDataPoint.sensor.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('quotient') &&
							eachDataPoint.hasOwnProperty('moisture') &&
							eachDataPoint.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.moisture);
							quotient.push(eachDataPoint.quotient);
							temperature.push(eachDataPoint.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('temperature') &&
							eachDataPoint.hasOwnProperty('humidity') &&
							eachDataPoint.hasOwnProperty('humidex')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							temperature.push(eachDataPoint.temperature);
							humidity.push(eachDataPoint.humidity);
							humidex.push(eachDataPoint.humidex);
							console.log('moisture', moisture);
						}
					} catch (e) {
						console.log(e);
					}
				}
				// const data = JSON.parse(res.data[`${row}`].stringifiedData.S);
				// for (const dataObj of res) {
				// 	empSal.push(parseInt(dataObj.employee_salary));
				// 	empAge.push(parseInt(dataObj.employee_age));
				// }
				setChartData11({
					labels: timestamp,
					datasets: [
						{
							label: 'temperature',
							data: temperature,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
						{
							label: 'humidity',
							data: humidity,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
					],
				});
			})
			.catch((err) => {
				console.log(err);
			});
	};

	const Chart12 = () => {
		let empSal = [];
		let empAge = [];
		let moisture = [];
		let quotient = [];
		let temperature = [];
		let timestamp = [];
		let humidex = [];
		let humidity = [];

		axiosMainHelper
			.get(
				`/rawtmpdata/?deviceID=${sensor12.uuidSource}&start_time=${moment()
					.startOf('day')
					.unix()}000&end_time=${dateNow}000`
				// `/rawtmpdata/?deviceID=${deviceId}`
			)
			.then((res) => {
				console.log('res', res);
				for (const row in res.data) {
					try {
						let eachDataPoint = JSON.parse(
							res.data[`${row}`].stringifiedData.S
						);
						console.log('eachDataPoint', eachDataPoint);
						if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor[0].hasOwnProperty('quotient') &&
							eachDataPoint.sensor[0].hasOwnProperty('moisture') &&
							eachDataPoint.sensor[0].hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor[0]', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor[0].moisture);
							quotient.push(eachDataPoint.sensor[0].quotient);
							temperature.push(eachDataPoint.sensor[0].temperature);
							console.log('moisture', moisture);
							// console.log('timestamp', timestamp);
						} else if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor.hasOwnProperty('quotient') &&
							eachDataPoint.sensor.hasOwnProperty('moisture') &&
							eachDataPoint.sensor.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor.moisture);
							quotient.push(eachDataPoint.sensor.quotient);
							temperature.push(eachDataPoint.sensor.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('quotient') &&
							eachDataPoint.hasOwnProperty('moisture') &&
							eachDataPoint.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.moisture);
							quotient.push(eachDataPoint.quotient);
							temperature.push(eachDataPoint.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('temperature') &&
							eachDataPoint.hasOwnProperty('humidity') &&
							eachDataPoint.hasOwnProperty('humidex')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							temperature.push(eachDataPoint.temperature);
							humidity.push(eachDataPoint.humidity);
							humidex.push(eachDataPoint.humidex);
							console.log('moisture', moisture);
						}
					} catch (e) {
						console.log(e);
					}
				}
				// const data = JSON.parse(res.data[`${row}`].stringifiedData.S);
				// for (const dataObj of res) {
				// 	empSal.push(parseInt(dataObj.employee_salary));
				// 	empAge.push(parseInt(dataObj.employee_age));
				// }
				setChartData12({
					labels: timestamp,
					datasets: [
						{
							label: 'temperature',
							data: temperature,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
						{
							label: 'humidity',
							data: humidity,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
					],
				});
			})
			.catch((err) => {
				console.log(err);
			});
	};

	const Chart13 = () => {
		let empSal = [];
		let empAge = [];
		let moisture = [];
		let quotient = [];
		let temperature = [];
		let timestamp = [];
		let humidex = [];
		let humidity = [];
		let PM_SP_UG_2_5 = [];
		let co2 = [];

		axiosMainHelper
			.get(
				`/rawtmpdata/?deviceID=${sensor13.uuidSource}&start_time=${moment()
					.startOf('day')
					.unix()}000&end_time=${dateNow}000`
				// `/rawtmpdata/?deviceID=${deviceId}`
			)
			.then((res) => {
				console.log('res', res);
				for (const row in res.data) {
					try {
						let eachDataPoint = JSON.parse(
							res.data[`${row}`].stringifiedData.S
						);
						console.log('eachDataPoint', eachDataPoint);
						if (
							eachDataPoint.hasOwnProperty('temperature') &&
							eachDataPoint.hasOwnProperty('humidity') &&
							eachDataPoint.hasOwnProperty('humidex') &&
							eachDataPoint.hasOwnProperty('co2') &&
							eachDataPoint.hasOwnProperty('AQI') &&
							eachDataPoint.AQI.hasOwnProperty('PM_SP_UG_2_5')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							co2.push(eachDataPoint.co2);
							PM_SP_UG_2_5.push(eachDataPoint.AQI.PM_SP_UG_2_5);
							temperature.push(eachDataPoint.temperature);
							humidity.push(eachDataPoint.humidity);
							humidex.push(eachDataPoint.humidex);
							console.log('moisture', moisture);
						}
					} catch (e) {
						console.log(e);
					}
				}
				// const data = JSON.parse(res.data[`${row}`].stringifiedData.S);
				// for (const dataObj of res) {
				// 	empSal.push(parseInt(dataObj.employee_salary));
				// 	empAge.push(parseInt(dataObj.employee_age));
				// }
				setChartData13({
					labels: timestamp,
					datasets: [
						{
							label: 'temperature',
							data: temperature,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
						{
							label: 'humidity',
							data: humidity,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
						{
							label: 'co2',
							data: co2,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
						{
							label: 'PM_SP_UG_2_5',
							data: PM_SP_UG_2_5,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
					],
				});
			})
			.catch((err) => {
				console.log(err);
			});
	};

	const Chart14 = () => {
		let empSal = [];
		let empAge = [];
		let moisture = [];
		let quotient = [];
		let temperature = [];
		let timestamp = [];
		let humidex = [];
		let humidity = [];
		let PM_SP_UG_2_5 = [];
		let co2 = [];

		axiosMainHelper
			.get(
				`/rawtmpdata/?deviceID=${sensor14.uuidSource}&start_time=${moment()
					.startOf('day')
					.unix()}000&end_time=${dateNow}000`
				// `/rawtmpdata/?deviceID=${deviceId}`
			)
			.then((res) => {
				console.log('res', res);
				for (const row in res.data) {
					try {
						let eachDataPoint = JSON.parse(
							res.data[`${row}`].stringifiedData.S
						);
						console.log('eachDataPoint', eachDataPoint);
						if (
							eachDataPoint.hasOwnProperty('temperature') &&
							eachDataPoint.hasOwnProperty('humidity') &&
							eachDataPoint.hasOwnProperty('humidex') &&
							eachDataPoint.hasOwnProperty('co2') &&
							eachDataPoint.hasOwnProperty('AQI') &&
							eachDataPoint.AQI.hasOwnProperty('PM_SP_UG_2_5')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							co2.push(eachDataPoint.co2);
							PM_SP_UG_2_5.push(eachDataPoint.AQI.PM_SP_UG_2_5);
							temperature.push(eachDataPoint.temperature);
							humidity.push(eachDataPoint.humidity);
							humidex.push(eachDataPoint.humidex);
							console.log('moisture', moisture);
						}
					} catch (e) {
						console.log(e);
					}
				}
				// const data = JSON.parse(res.data[`${row}`].stringifiedData.S);
				// for (const dataObj of res) {
				// 	empSal.push(parseInt(dataObj.employee_salary));
				// 	empAge.push(parseInt(dataObj.employee_age));
				// }
				setChartData14({
					labels: timestamp,
					datasets: [
						{
							label: 'temperature',
							data: temperature,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
						{
							label: 'humidity',
							data: humidity,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
						{
							label: 'co2',
							data: co2,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
						{
							label: 'PM_SP_UG_2_5',
							data: PM_SP_UG_2_5,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
					],
				});
			})
			.catch((err) => {
				console.log(err);
			});
	};

	const Chart15 = () => {
		let empSal = [];
		let empAge = [];
		let moisture = [];
		let quotient = [];
		let temperature = [];
		let timestamp = [];
		let humidex = [];
		let humidity = [];

		axiosMainHelper
			.get(
				`/rawtmpdata/?deviceID=${sensor15.uuidSource}&start_time=${moment()
					.startOf('day')
					.unix()}000&end_time=${dateNow}000`
				// `/rawtmpdata/?deviceID=${deviceId}`
			)
			.then((res) => {
				console.log('res', res);
				for (const row in res.data) {
					try {
						let eachDataPoint = JSON.parse(
							res.data[`${row}`].stringifiedData.S
						);
						console.log('eachDataPoint', eachDataPoint);
						if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor[0].hasOwnProperty('quotient') &&
							eachDataPoint.sensor[0].hasOwnProperty('moisture') &&
							eachDataPoint.sensor[0].hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor[0]', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor[0].moisture);
							quotient.push(eachDataPoint.sensor[0].quotient);
							temperature.push(eachDataPoint.sensor[0].temperature);
							console.log('moisture', moisture);
							// console.log('timestamp', timestamp);
						} else if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor.hasOwnProperty('quotient') &&
							eachDataPoint.sensor.hasOwnProperty('moisture') &&
							eachDataPoint.sensor.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor.moisture);
							quotient.push(eachDataPoint.sensor.quotient);
							temperature.push(eachDataPoint.sensor.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('quotient') &&
							eachDataPoint.hasOwnProperty('moisture') &&
							eachDataPoint.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.moisture);
							quotient.push(eachDataPoint.quotient);
							temperature.push(eachDataPoint.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('temperature') &&
							eachDataPoint.hasOwnProperty('humidity') &&
							eachDataPoint.hasOwnProperty('humidex')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							temperature.push(eachDataPoint.temperature);
							humidity.push(eachDataPoint.humidity);
							humidex.push(eachDataPoint.humidex);
							console.log('moisture', moisture);
						}
					} catch (e) {
						console.log(e);
					}
				}
				// const data = JSON.parse(res.data[`${row}`].stringifiedData.S);
				// for (const dataObj of res) {
				// 	empSal.push(parseInt(dataObj.employee_salary));
				// 	empAge.push(parseInt(dataObj.employee_age));
				// }
				setChartData15({
					labels: timestamp,
					datasets: [
						{
							label: 'temperature',
							data: temperature,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
						{
							label: 'humidity',
							data: humidity,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
					],
				});
			})
			.catch((err) => {
				console.log(err);
			});
	};
	// const getTransactionData = async () => {
	// 	let moisture = [];
	// 	let quotient = [];
	// 	let temperature = [];
	// 	let timestamp = [];
	// 	let humidex = [];
	// 	let humidity = [];
	// 	let outputCSV = [];
	// 	let outputData = {};
	// 	// 'api' just wraps axios with some setting specific to our app. the important thing here is that we use .then to capture the table response data, update the state, and then once we exit that operation we're going to click on the csv download link using the ref
	// 	await axiosMainHelper
	// 		.get(
	// 			`/rawtmpdata/?deviceID=${deviceID}&start_time=${date2DaysAgoFromNow}000&end_time=${dateNow}000`
	// 		)
	// 		.then((res) => {
	// 			console.log('res', res);
	// 			for (const row in res.data) {
	// 				try {
	// 					let eachDataPoint = JSON.parse(
	// 						res.data[`${row}`].stringifiedData.S
	// 					);
	// 					console.log('eachDataPoint', eachDataPoint);
	// 					if (
	// 						eachDataPoint.hasOwnProperty('sensor') &&
	// 						eachDataPoint.sensor[0].hasOwnProperty('quotient') &&
	// 						eachDataPoint.sensor[0].hasOwnProperty('moisture') &&
	// 						eachDataPoint.sensor[0].hasOwnProperty('temperature')
	// 					) {
	// 						console.log('eachDataPoint sensor[0]', eachDataPoint);
	// 						timestamp.push(
	// 							`${eachDataPoint.timestamp}_${moment
	// 								.unix(eachDataPoint.timestamp)
	// 								.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
	// 						);
	// 						moisture.push(eachDataPoint.sensor[0].moisture);
	// 						quotient.push(eachDataPoint.sensor[0].quotient);
	// 						temperature.push(eachDataPoint.sensor[0].temperature);
	// 						console.log('moisture', moisture);
	// 						// console.log('timestamp', timestamp);
	// 					} else if (
	// 						eachDataPoint.hasOwnProperty('sensor') &&
	// 						eachDataPoint.sensor.hasOwnProperty('quotient') &&
	// 						eachDataPoint.sensor.hasOwnProperty('moisture') &&
	// 						eachDataPoint.sensor.hasOwnProperty('temperature')
	// 					) {
	// 						console.log('eachDataPoint sensor', eachDataPoint);
	// 						timestamp.push(
	// 							`${eachDataPoint.timestamp}_${moment
	// 								.unix(eachDataPoint.timestamp)
	// 								.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
	// 						);
	// 						moisture.push(eachDataPoint.sensor.moisture);
	// 						quotient.push(eachDataPoint.sensor.quotient);
	// 						temperature.push(eachDataPoint.sensor.temperature);
	// 						console.log('moisture', moisture);
	// 					} else if (
	// 						eachDataPoint.hasOwnProperty('quotient') &&
	// 						eachDataPoint.hasOwnProperty('moisture') &&
	// 						eachDataPoint.hasOwnProperty('temperature')
	// 					) {
	// 						console.log('eachDataPoint sensor', eachDataPoint);
	// 						timestamp.push(
	// 							`${eachDataPoint.timestamp}_${moment
	// 								.unix(eachDataPoint.timestamp)
	// 								.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
	// 						);
	// 						moisture.push(eachDataPoint.moisture);
	// 						quotient.push(eachDataPoint.quotient);
	// 						temperature.push(eachDataPoint.temperature);
	// 						console.log('moisture', moisture);
	// 					} else if (
	// 						eachDataPoint.hasOwnProperty('temperature') &&
	// 						eachDataPoint.hasOwnProperty('humidity') &&
	// 						eachDataPoint.hasOwnProperty('humidex')
	// 					) {
	// 						console.log('eachDataPoint sensor', eachDataPoint);
	// 						timestamp.push(
	// 							`${eachDataPoint.timestamp}_${moment
	// 								.unix(eachDataPoint.timestamp)
	// 								.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
	// 						);
	// 						temperature.push(eachDataPoint.temperature);
	// 						humidity.push(eachDataPoint.humidity);
	// 						humidex.push(eachDataPoint.humidex);
	// 						console.log('moisture', moisture);
	// 						outputData = {
	// 							deviceID: deviceID,
	// 							timestamp: `${eachDataPoint.timestamp}_${moment
	// 								.unix(eachDataPoint.timestamp)
	// 								.format('dddd, MMMM Do, YYYY h:mm:ss A')}`,
	// 							temperature: eachDataPoint.temperature,
	// 							humidity: eachDataPoint.humidity,
	// 						};
	// 					}
	// 					outputCSV.push(outputData);
	// 				} catch (e) {
	// 					console.log(e);
	// 				}
	// 			}
	// 			setTransactionData(outputCSV);
	// 		})
	// 		.catch((e) => console.log(e));
	// 	// console.log('csvLink', csvLink);
	// 	// csvLink.current.link.click();
	// };

	useEffect(() => {
		Chart();
		Chart2();
		Chart3();
		Chart4();
		Chart5();
		Chart6();
		Chart7();
		Chart8();
		Chart9();
		Chart10();
		Chart11();
		Chart12();
		Chart13();
		Chart14();
		Chart15();
	}, []);
	return (
		<div className="App">
			<h1>
				Reading of {title} {sensor1.name} between {date2DaysAgoFromNowHumanRead}{' '}
				to
				{dateNowHumanRead}
			</h1>
			<div>
				<Line
					data={chartData1}
					options={{
						responsive: true,
						title: { text: { title }, display: true },
						scales: {
							yAxes: {
								ticks: {
									beginAtZero: true,
								},
							},
						},
					}}
				/>
			</div>
			<h1>
				Reading of {title} {sensor2.name} between {date2DaysAgoFromNowHumanRead}{' '}
				to
				{dateNowHumanRead}
			</h1>
			<div>
				<Line
					data={chartData2}
					options={{
						responsive: true,
						title: { text: { title }, display: true },
						scales: {
							yAxes: {
								ticks: {
									beginAtZero: true,
								},
							},
						},
					}}
				/>
			</div>
			<h1>
				Reading of {title} {sensor3.name} between {date2DaysAgoFromNowHumanRead}{' '}
				to
				{dateNowHumanRead}
			</h1>
			<div>
				<Line
					data={chartData3}
					options={{
						responsive: true,
						title: { text: { title }, display: true },
						scales: {
							yAxes: {
								ticks: {
									beginAtZero: true,
								},
							},
						},
					}}
				/>
			</div>
			<h1>
				Reading of {title} {sensor4.name} between {date2DaysAgoFromNowHumanRead}{' '}
				to
				{dateNowHumanRead}
			</h1>
			<div>
				<Line
					data={chartData4}
					options={{
						responsive: true,
						title: { text: { title }, display: true },
						scales: {
							yAxes: {
								ticks: {
									beginAtZero: true,
								},
							},
						},
					}}
				/>
			</div>
			<h1>
				Reading of {title} {sensor5.name} between {date2DaysAgoFromNowHumanRead}{' '}
				to
				{dateNowHumanRead}
			</h1>
			<div>
				<Line
					data={chartData5}
					options={{
						responsive: true,
						title: { text: { title }, display: true },
						scales: {
							yAxes: {
								ticks: {
									beginAtZero: true,
								},
							},
						},
					}}
				/>
			</div>
			<h1>
				Reading of {title} {sensor6.name} between {date2DaysAgoFromNowHumanRead}{' '}
				to
				{dateNowHumanRead}
			</h1>
			<div>
				<Line
					data={chartData6}
					options={{
						responsive: true,
						title: { text: { title }, display: true },
						scales: {
							yAxes: {
								ticks: {
									beginAtZero: true,
								},
							},
						},
					}}
				/>
			</div>
			<h1>
				Reading of {title} {sensor7.name} between {date2DaysAgoFromNowHumanRead}{' '}
				to
				{dateNowHumanRead}
			</h1>
			<div>
				<Line
					data={chartData7}
					options={{
						responsive: true,
						title: { text: { title }, display: true },
						scales: {
							yAxes: {
								ticks: {
									beginAtZero: true,
								},
							},
						},
					}}
				/>
			</div>
			<h1>
				Reading of {title} {sensor8.name} between {date2DaysAgoFromNowHumanRead}{' '}
				to
				{dateNowHumanRead}
			</h1>
			<div>
				<Line
					data={chartData8}
					options={{
						responsive: true,
						title: { text: { title }, display: true },
						scales: {
							yAxes: {
								ticks: {
									beginAtZero: true,
								},
							},
						},
					}}
				/>
			</div>
			<h1>
				Reading of {title} {sensor9.name} between {date2DaysAgoFromNowHumanRead}{' '}
				to
				{dateNowHumanRead}
			</h1>
			<div>
				<Line
					data={chartData9}
					options={{
						responsive: true,
						title: { text: { title }, display: true },
						scales: {
							yAxes: {
								ticks: {
									beginAtZero: true,
								},
							},
						},
					}}
				/>
			</div>
			<h1>
				Reading of {title} {sensor10.name} between{' '}
				{date2DaysAgoFromNowHumanRead} to
				{dateNowHumanRead}
			</h1>
			<div>
				<Line
					data={chartData10}
					options={{
						responsive: true,
						title: { text: { title }, display: true },
						scales: {
							yAxes: {
								ticks: {
									beginAtZero: true,
								},
							},
						},
					}}
				/>
			</div>
			<h1>
				Reading of {title} {sensor11.name} between{' '}
				{date2DaysAgoFromNowHumanRead} to
				{dateNowHumanRead}
			</h1>
			<div>
				<Line
					data={chartData11}
					options={{
						responsive: true,
						title: { text: { title }, display: true },
						scales: {
							yAxes: {
								ticks: {
									beginAtZero: true,
								},
							},
						},
					}}
				/>
			</div>
			<h1>
				Reading of {title} {sensor12.name} between{' '}
				{date2DaysAgoFromNowHumanRead} to
				{dateNowHumanRead}
			</h1>
			<div>
				<Line
					data={chartData12}
					options={{
						responsive: true,
						title: { text: { title }, display: true },
						scales: {
							yAxes: {
								ticks: {
									beginAtZero: true,
								},
							},
						},
					}}
				/>
			</div>
			<h1>
				Reading of {title} {sensor13.name} between{' '}
				{date2DaysAgoFromNowHumanRead} to
				{dateNowHumanRead}
			</h1>
			<div>
				<Line
					data={chartData13}
					options={{
						responsive: true,
						title: { text: { title }, display: true },
						scales: {
							yAxes: {
								ticks: {
									beginAtZero: true,
								},
							},
						},
					}}
				/>
			</div>
			<h1>
				Reading of {title} {sensor14.name} between{' '}
				{date2DaysAgoFromNowHumanRead} to
				{dateNowHumanRead}
			</h1>
			<div>
				<Line
					data={chartData14}
					options={{
						responsive: true,
						title: { text: { title }, display: true },
						scales: {
							yAxes: {
								ticks: {
									beginAtZero: true,
								},
							},
						},
					}}
				/>
			</div>
			<h1>
				Reading of {title} {sensor15.name} between{' '}
				{date2DaysAgoFromNowHumanRead} to
				{dateNowHumanRead}
			</h1>
			<div>
				<Line
					data={chartData15}
					options={{
						responsive: true,
						title: { text: { title }, display: true },
						scales: {
							yAxes: {
								ticks: {
									beginAtZero: true,
								},
							},
						},
					}}
				/>
			</div>
			{/* <div>
				<Button onClick={getTransactionData}>
					Download {filename} between {date2DaysAgoFromNowHumanRead} to
					{dateNowHumanRead}
				</Button>
				<CSVLink
					data={transactionData}
					filename={filename}
					className="hidden"
					ref={csvLink}
					target="_blank"
				/>
			</div> */}
		</div>
	);
};

export default DynamicChart;
